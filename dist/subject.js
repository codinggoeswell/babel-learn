"use strict";

class Subject {
  constructor(name) {
    this.name = name;
  }

  getName() {
    return this.name;
  }

}