class Student {
  constructor(name) {
    this.name = name;
  }

  getName() {
    return this.name;
  }

  getAge() {
    return 1;
  }

}